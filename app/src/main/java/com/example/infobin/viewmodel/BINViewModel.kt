package com.example.infobin.viewmodel

import androidx.lifecycle.*
import com.example.infobin.database.BINRepository
import com.example.infobin.model.Bin
import kotlinx.coroutines.launch

class BINViewModel(private val repository: BINRepository) : ViewModel() {

    //из ViewModel (UI) данные должны идти в Repository (BINRepository)

    // Using LiveData and caching what allWords returns has several benefits:
    // - We can put an observer on the data (instead of polling for changes) and only update the
    //   the UI when the data actually changes.
    // - Repository is completely separated from the UI through the ViewModel.
    val allBins: LiveData<List<Bin>> = repository.allData.asLiveData()


    /**
     * Launching a new coroutine to insert the data in a non-blocking way
     */
    fun insert(bin: Bin) = viewModelScope.launch {
        repository.insert(bin)
    }
    fun deleteBin(bin: Bin) = viewModelScope.launch {
        repository.deleteBin(bin)
    }

}


class BINViewModelFactory(private val repository: BINRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(BINViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return BINViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}