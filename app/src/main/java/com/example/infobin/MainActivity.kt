package com.example.infobin

import android.Manifest.permission.CALL_PHONE
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.icu.lang.UProperty.DASH
import android.net.Uri
import android.os.Bundle
import android.provider.SyncStateContract.Constants
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.widget.doAfterTextChanged
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.infobin.databinding.ActivityMainBinding
import com.example.infobin.model.Bin
import com.example.infobin.viewmodel.BINViewModel
import com.example.infobin.viewmodel.BINViewModelFactory
import org.json.JSONObject

var latitude = ""
var longitude = ""

class MainActivity : AppCompatActivity() {

    private val binViewModel: BINViewModel by viewModels {
        BINViewModelFactory((application as BINsApplication).repository)
    }

    private lateinit var binding: ActivityMainBinding

    @SuppressLint("SuspiciousIndentation")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        var numb: String

        binding.etEnterBin.doAfterTextChanged { text ->
            val formattedText = text.toString().replace(" ", "").chunked(4).joinToString(" ")
            if (formattedText != text.toString()) {
                binding.etEnterBin.setText(formattedText)

                binding.etEnterBin.setSelection(binding.etEnterBin.length())
            }
        }

        binding.buttCheckBin.setOnClickListener {
            if (binding.etEnterBin.text.toString().replace(" ", "").equals("")) {
                Toast.makeText(this, "No information", Toast.LENGTH_SHORT).show()
            } else {
                numb = binding.etEnterBin.text.toString().replace(" ", "")
                getData(numb)
                Log.d("Response", "Result:$numb")
            }
        }

        binding.bRequestsList.setOnClickListener {
            val intent = Intent(this@MainActivity, BINInfoActivity::class.java)
            startActivity(intent)
        }

//        if (shop.aboutPhone3.equals("")) {
//            binding.get().phone3TextView.setText(Constants.DASH);
//            binding.get().phone3TextView.setVisibility(View.GONE);
//        } else {
//            binding.get().phone3TextView.setText(shop.aboutPhone3);
//            binding.get().phone3TextView.setVisibility(View.VISIBLE);
//        }

        binding.tvSite.setOnClickListener {
            if (binding.tvSite.text.toString().equals("No information")) {
                Toast.makeText(this, "No information", Toast.LENGTH_SHORT).show()
            } else {
                val urlIntent =
                    Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://" + binding.tvSite.text.toString()))
                startActivity(urlIntent)
            }
        }

        binding.tvPhone.setOnClickListener {
            if (binding.tvSite.text.toString().equals("No information")) {
                Toast.makeText(this, "No information", Toast.LENGTH_SHORT).show()
            } else {
                val phoneNum =
                    binding.tvPhone.text.toString().replace(" ", "").replace("(", "")
                        .replace(")", "")
                        .replace("-", "")
                val phoneIntent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:$phoneNum"))
                startActivity(phoneIntent)
            }
        }

        binding.tvCountry.setOnClickListener {
            if (binding.tvCountry.text.toString().equals("...")) {
                Toast.makeText(this, "No information", Toast.LENGTH_SHORT).show()
            } else {
                val mapIntent = Intent(Intent.ACTION_VIEW, Uri.parse("geo:$latitude,$longitude"))
                startActivity(mapIntent)
            }
        }
    }

    private fun getData(binNum: String) {
        val url = "https://lookup.binlist.net/$binNum"
        val mRequestQueue = Volley.newRequestQueue(this)
        val stringRequest = StringRequest(Request.Method.GET, url,
            { result ->
                Log.d("Response", "Result:$result")
                parseBINData(result)
                sendData()
            },
            { error ->
                Log.d("Response", "Result:$error")
                Toast.makeText(this, "No information", Toast.LENGTH_SHORT).show()
            })

        mRequestQueue.add(stringRequest)
    }

    private fun parseBINData(result: String) {
        val replyIntent = Intent()
        replyIntent.putExtra("num", binding.etEnterBin.text.toString())

        val mainObject = JSONObject(result)

        if (mainObject.has("country") && !mainObject.isNull("country")) {
            binding.tvCountry.text = mainObject.getJSONObject("country").optString("name")
            if (binding.tvCountry.text == "") {
                binding.tvCountry.text = "No information"
            }
        } else {
            binding.tvCountry.text = "No information"
        }

        if (mainObject.has("country") && !mainObject.isNull("country")) {
            latitude = mainObject.getJSONObject("country").optString("latitude")
            longitude = mainObject.getJSONObject("country").optString("longitude")
            if (latitude == "" || longitude == "") {
                latitude = "No information"
                longitude = "No information"
            }
        }

        if (mainObject.has("country") && !mainObject.isNull("country")) {
            binding.tvFlag.text = mainObject.getJSONObject("country").optString("emoji")
        } else {
            binding.tvFlag.text = ""
        }

        if (mainObject.has("scheme") && !mainObject.isNull("scheme")) {
            binding.tvNetwork.text = mainObject.optString("scheme")
        } else {
            binding.tvNetwork.text = "No information"
        }

        if (mainObject.has("type") && !mainObject.isNull("type")) {
            binding.tvTypeCard.text = mainObject.optString("type")
        } else {
            binding.tvTypeCard.text = "No information"
        }

        if (mainObject.has("brand") && !mainObject.isNull("brand")) {
            binding.tvBrandCard.text = mainObject.optString("brand")
        } else {
            binding.tvBrandCard.text = "No information"
        }

        if (mainObject.has("prepaid") && !mainObject.isNull("prepaid")) {
            binding.tvPrepaid.text = mainObject.optString("prepaid")
        } else {
            binding.tvPrepaid.text = "No information"
        }

        if (mainObject.has("bank") && !mainObject.isNull("bank")) {
            binding.tvBank.text = mainObject.getJSONObject("bank").optString("name")
            if (binding.tvBank.text == "") {
                binding.tvBank.text = "No information"
            }
        } else {
            binding.tvBank.text = "No information"
        }

        if (mainObject.has("bank") && !mainObject.isNull("bank")) {
            binding.tvSite.text = mainObject.getJSONObject("bank").optString("url")
            if (binding.tvSite.text == "") {
                binding.tvSite.text = "No information"
            }
        } else {
            binding.tvSite.text = "No information"
        }

        if (mainObject.has("bank") && !mainObject.isNull("bank")) {
            binding.tvPhone.text = mainObject.getJSONObject("bank").optString("phone")
            if (binding.tvPhone.text == "") {
                binding.tvPhone.text = "No information"
            }
        } else {
            binding.tvPhone.text = "No information"
        }

    }

    private fun sendData() {
        val number = binding.etEnterBin.text.toString()
        val country = binding.tvCountry.text.toString()
        val flag = binding.tvFlag.text.toString()
        val network = binding.tvNetwork.text.toString()
        val typeCard = binding.tvTypeCard.text.toString()
        val branCard = binding.tvBrandCard.text.toString()
        val prepaid = binding.tvPrepaid.text.toString()
        val bank = binding.tvBank.text.toString()

        val siteBank = binding.tvSite.text.toString()
        val phoneBank = binding.tvPhone.text.toString()

        val bin = Bin(number,
            country,
            flag,
            network,
            typeCard,
            branCard,
            prepaid,
            bank,
            siteBank,
            phoneBank)
        binViewModel.insert(bin)
    }

    companion object {
        const val EXTRA_REPLY = "com.example.android.binning.REPLY"
    }
}
