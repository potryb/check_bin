package com.example.infobin.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.infobin.model.Bin
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch


// Annotates class to be a Room Database with a table (entity) of the Word class
@Database(entities = [Bin::class], version = 1, exportSchema = false)
abstract class BINRoomDatabase : RoomDatabase() {

    abstract fun binDao(): BINDao

//    private class BinDatabaseCallback(
//        private val scope: CoroutineScope
//    ) : RoomDatabase.Callback() {
//
//        override fun onCreate(db: SupportSQLiteDatabase) {
//            super.onCreate(db)
//            INSTANCE?.let { database ->
//                scope.launch {
//                    //var binDao = database.binDao()
//                    //populateDatabase(database.binDao())
//                }
//            }
//        } //это мне не нужно

////        suspend fun populateDatabase(binDao: BINDao) {
////        // Delete all content here.
////        binDao.deleteAll()
////
////            // Add sample words.
////            var bin = Bin("ddds", "ddd","sasa","ggg","ddd","ffff","aaa","ss", "dsdsd","33223")
////            binDao.addBin(bin)
////
////            bin = Bin("AAAAds", "ddddsdsd","sasaaaaa","ggg","ddd","ffff","aaa","dsds","esdsd","434343")
////            binDao.addBin(bin)
////
////        }
//
   // }

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: BINRoomDatabase? = null

        fun getDatabase(
            context: Context,
            scope: CoroutineScope
        ): BINRoomDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) { // https://www.youtube.com/watch?v=lwAvI3WDXBY&list=PLSrm9z4zp4mEPOfZNV9O-crOhoMa0G2-o 10:50
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    BINRoomDatabase::class.java,
                    "bin_database"
                )
                    //.fallbackToDestructiveMigration()
                    //.addCallback(BinDatabaseCallback(scope))
                     //.createFromAsset("database/bin.db")
                    .build()
                INSTANCE = instance

                instance // return instance
            }
        }
    }
}