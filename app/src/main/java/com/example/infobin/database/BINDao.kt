package com.example.infobin.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.infobin.model.Bin
import kotlinx.coroutines.flow.Flow

@Dao
interface BINDao {

    // Добавление Bin в бд
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addBin(bin: Bin)

    // Получение всех Bin из таблицы/чтение списка бинов
    @Query("SELECT * FROM bin_table ORDER BY id ASC")
    fun readAllData() : Flow<List<Bin>>

    //Удаление Bin из бд
    @Query("DELETE FROM bin_table")
    suspend fun deleteAll()

    @Delete
    suspend fun deleteBin(bin: Bin)

}