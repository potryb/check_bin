package com.example.infobin.database

import androidx.annotation.WorkerThread
import com.example.infobin.model.Bin
import kotlinx.coroutines.flow.Flow

class BINRepository(private val binDao: BINDao) {

    //Из Repository данные идут  Model (Bin), по сути репозиторий нужен для архетиктуры
    //но выполняет работу dao


    // Room executes all queries on a separate thread.
    // Observed Flow will notify the observer when the data has changed.
    val allData: Flow<List<Bin>> = binDao.readAllData() //readAllData

    // By default Room runs suspend queries off the main thread, therefore, we don't need to
    // implement anything else to ensure we're not doing long running database work
    // off the main thread.

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(bin: Bin) { //addBin
        binDao.addBin(bin)
    }
    
    suspend fun deleteBin(bin: Bin) = binDao.deleteBin(bin)

}