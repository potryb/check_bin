package com.example.infobin

import android.content.Intent
import android.os.Bundle
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.infobin.adapter.BINListAdapter
import com.example.infobin.model.Bin
import com.example.infobin.viewmodel.BINViewModel
import com.example.infobin.viewmodel.BINViewModelFactory


class BINInfoActivity : AppCompatActivity()//, BINListAdapter.OnClickListener() {
{
    private val binViewModel: BINViewModel by viewModels {
        BINViewModelFactory((application as BINsApplication).repository)
    }
    private var launcher:ActivityResultLauncher<Intent>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bininfo)


        //ToolBar
        val tbMenu = findViewById<Toolbar>(R.id.tb_menu)
        setSupportActionBar(tbMenu as Toolbar?)
        //tbMenu.setDispl
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        //Установка RecyclerView
        val recyclerView = findViewById<RecyclerView>(R.id.rv_binsList)
        val adapter = BINListAdapter()//BINListAdapter.OnClickListener() )
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        //configureRecyclerView()

        binViewModel.allBins.observe(this) { bins ->
            // Update the cached copy of the words in the adapter.
            bins?.let { adapter.submitList(it) }
        }
    }

//    override fun onLongClick(bin: Bin) {
//        TODO("Not yet implemented")
//    }
//
//    override fun onClick(bin: Bin) {
//        TODO("Not yet implemented")
//    }

//    private fun configureRecyclerView() {
//
//        val recyclerView = findViewById<RecyclerView>(R.id.rv_binsList)
//        val adapter = BINListAdapter()
//        recyclerView.adapter = adapter
//        recyclerView.layoutManager = LinearLayoutManager(this)
//
//       // adapter.addBin(
//       }
}

