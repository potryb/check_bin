package com.example.infobin.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.infobin.databinding.ItemListBinding
import com.example.infobin.model.Bin

class BINListAdapter(
    //private val itemClickListener: OnClickListener,
) : ListAdapter<Bin, BINListAdapter.BinViewHolder>(BinsComparator()) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BinViewHolder {
        return BinViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: BinViewHolder, position: Int) {
        holder.bind(getItem(position))

        holder.itemView.setOnClickListener {
            //itemClickListener.onClick(getItem(position))
            //itemClickListener.pop
        }
    }

//    interface ClickListener {
//        fun onLongClick(bin: Bin)
//
//        fun onClick(bin: Bin)
//    }

//    class OnClickListener() {
//        fun onClick(bin: Bin) = clickListener(bin)
//    }

    class BinViewHolder(private val binding: ItemListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(bin: Bin) = with(binding) {

            tvNumber.text = bin.num.toString()
            tvCountry.text = bin.country
            tvFlag.text = bin.flag
            tvNetwork.text = bin.network
            tvTypeCard.text = bin.typeCard
            tvBrandCard.text = bin.brandCard
            tvPrepaid.text = bin.prepaid
            tvBank.text = bin.bank

            tvSite.text = bin.site
            tvPhone.text = bin.phone
        }

        companion object {
            fun create(parent: ViewGroup): BinViewHolder {
                return BinViewHolder(ItemListBinding
                    .inflate(LayoutInflater.from(parent.context), parent, false))
            }
        }
    }

    class BinsComparator : DiffUtil.ItemCallback<Bin>() {
        override fun areItemsTheSame(oldItem: Bin, newItem: Bin): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Bin, newItem: Bin): Boolean {
            return oldItem == newItem
        }
    }
}