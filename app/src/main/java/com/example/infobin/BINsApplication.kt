package com.example.infobin

import android.app.Application
import com.example.infobin.database.BINRepository
import com.example.infobin.database.BINRoomDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class BINsApplication : Application() {
    // No need to cancel this scope as it'll be torn down with the process
    val applicationScope = CoroutineScope(SupervisorJob())

    // Using by lazy so the database and the repository are only created when they're needed
    // rather than when the application starts
    val database: BINRoomDatabase by lazy { BINRoomDatabase.getDatabase(this,applicationScope) }
    val repository by lazy { BINRepository(database.binDao()) }
}