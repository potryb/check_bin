package com.example.infobin.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey


@Entity (
    tableName = "bin_table",
    indices = [
        Index("bin", unique = true)
    ]
)

data class Bin(

    //@PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo (name = "bin") val num: String? = null, // Bin number
    val country: String? = null,                        // Country
    val flag: String? = null,                           // Flag of country
    val network: String? = null,                        // Visa/mastercard
    val typeCard: String? = null,                       // Debit/credit
    val brandCard: String? = null,                      // Business/traditional
    val prepaid: String? = null,                        // Предоплата
    val bank: String? = null,                           // Name of bank

    val site: String? = null,
    val phone: String? = null,

)

{
    @PrimaryKey(autoGenerate = true) var id: Int = 0
}